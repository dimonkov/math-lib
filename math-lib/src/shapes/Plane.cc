#include "math-lib/shapes/Plane.h"
#include "math-lib/vector3.h"

#include <optional>

namespace math::shapes
{
	std::optional<double> Plane::intersect(const vector3 & obj_origin, const ray & ray) const
	{
		vector3 norm = this->Normal;

		double denom = norm.dot(ray.direction);

		if (denom > 1e-6f)
		{
			vector3 v = obj_origin - ray.origin;

			double distance = v.dot(norm) / denom;

			if (distance >= 0)
				return distance;
			else
				return std::nullopt;
		}

		return std::nullopt;
	}
	vector3 Plane::surface_normal(const vector3& obj_origin, const vector3& hit_point) const
	{
		return -Normal;
	}
}
