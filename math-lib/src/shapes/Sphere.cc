#include "math-lib/shapes/Sphere.h"
#define _USE_MATH_DEFINES
#include <math.h>

namespace math::shapes
{
	std::optional<double> Sphere::intersect(const vector3 & obj_origin, const ray & ray) const
	{
		vector3 l = obj_origin - ray.origin;
		double adj = l.dot(ray.direction);
		double d2 = l.dot(l) - (adj * adj);
		double radius2 = this->Radius * this->Radius;
		if (d2 > radius2){
			return std::nullopt;
		}
		double thc = sqrt((radius2 - d2));
		double t0 = adj - thc;
		double t1 = adj + thc;

		if (t0 < 0.0 && t1 < 0.0) {
			return std::nullopt;
		}

		double distance = t0 < t1 ? t0 : t1;

		
		return distance;
	}
	vector3 Sphere::surface_normal(const vector3& obj_origin, const vector3& hit_point) const
	{
		return (hit_point - obj_origin).normalize();
	}
}
