#include "math-lib/vector2.h"

namespace math
{
	vector2::vector2(double x, double y) : X(x), Y(y) { }

	vector2::vector2() : X(0), Y(0) { }

	vector2 vector2::operator+(const vector2 & vec) const
	{
		return vector2(this->X + vec.X, this->Y + vec.Y);
	}

	vector2 vector2::operator-(const vector2 & vec) const
	{
		return vector2(this->X - vec.X, this->Y - vec.Y);
	}

	vector2 vector2::operator*(int n) const
	{
		return vector2(this->X * n, this->Y * n);
	}

	vector2 vector2::operator/(int n) const
	{
		return vector2(this->X / n, this->Y / n);
	}

}
