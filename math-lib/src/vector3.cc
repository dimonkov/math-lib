#define _USE_MATH_DEFINES
#include <math.h>

#include "math-lib/vector3.h"

namespace math
{
	vector3::vector3(double x, double y, double z) : X(x), Y(y), Z(z) { }

	vector3::vector3(double x, double y) : X(x), Y(y), Z(0.0f) { }

	vector3::vector3(double x) : X(x), Y(0.0f), Z(0.0f) { }



	vector3::vector3() : X(0.0f), Y(0.0f), Z(0.0f) { }

	vector3 vector3::operator+(const vector3 & vec) const
	{
		return vector3(this->X + vec.X, this->Y + vec.Y, this->Z + vec.Z);
	}

	vector3 vector3::operator-(const vector3 & vec) const
	{
		return vector3(this->X - vec.X, this->Y - vec.Y, this->Z - vec.Z);
	}

	vector3 vector3::operator-() const
	{
		return vector3(-X, -Y, -Z);
	}

	vector3 vector3::operator*(double n) const
	{
		return vector3(this->X * n, this->Y * n, this->Z * n);
	}

	vector3 vector3::operator/(double n) const
	{
		return vector3(this->X / n, this->Y / n, this->Z / n);
	}
	double vector3::length() const
	{
		return sqrt(this->sqr_length());
	}
	double vector3::sqr_length() const
	{
		return X * X + Y * Y + Z * Z;
	}
	vector3 vector3::normalize() const
	{
		double len = this->length();
		return this->operator/(len);
	}
	double vector3::dot(const vector3 & other)
	{
		return X * other.X + Y * other.Y + Z * other.Z;
	}
	vector3 vector3::cross(const vector3 & other)
	{
		return vector3(Y * other.Z - Z * other.Y,
					   Z * other.X - X * other.Z,
					   X * other.Y - Y * other.X);
	}
}
