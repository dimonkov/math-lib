#include "math-lib/area.h"
#include <algorithm>

namespace math
{
	area::area() : A(vector2(0, 0)), B(vector2(0, 0)) { }

	area::area(vector2 a, vector2 b)
	{
		double minx = std::min(a.X, b.X);
		double maxx = std::max(a.X, b.X);

		double miny = std::min(a.Y, b.Y);
		double maxy = std::max(a.Y, b.Y);

		this->A = vector2(minx, miny);
		this->B = vector2(maxx, maxy);
	}

	bool area::Intersects(vector2 coord) const
	{
		return coord.X >= A.X && coord.X <= B.X && coord.Y >= A.Y && coord.Y <= B.Y;
	}

}
