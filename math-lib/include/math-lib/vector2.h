#ifndef MATH_VECTOR2_H_
#define MATH_VECTOR2_H_

namespace math
{
	//Two coordinates representing a point on the plane
	class vector2
	{
	public:
		vector2(double x, double y);
		vector2();
		double X, Y;

		vector2 operator+(const vector2& vec) const;
		vector2 operator-(const vector2& vec) const;
		vector2 operator*(int n) const;
		vector2 operator/(int n) const;
	};

}

#endif // !MATH_VECTOR2_H_
