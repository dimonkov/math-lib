#ifndef MATH_SHAPES_SPHERE_H_  
#define MATH_SHAPES_SPHERE_H_
#include "math-lib/shapes/Shape.h"

namespace math::shapes
{
	class Sphere : public Shape
	{
	public:
		double Radius = 0;

		std::optional<double> intersect(const vector3& obj_origin, const ray& ray) const override;

		vector3 surface_normal(const vector3& obj_origin, const vector3& hit_point) const override;
	};
}
#endif // !MATH_SHAPES_SPHERE_H_
