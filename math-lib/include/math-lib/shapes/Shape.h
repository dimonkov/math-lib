#ifndef MATH_SHAPES_H_  
#define MATH_SHAPES_H_
#include <optional>

#include "math-lib/vector3.h"
#include "math-lib/ray.h"


namespace math::shapes
{
	class Shape
	{
	public:
		//Will return an intersection distance or std::nullopt if no intercection occured
		virtual std::optional<double> intersect(const vector3& obj_origin, const ray& ray) const = 0;
		virtual vector3 surface_normal(const vector3& obj_origin, const vector3& hit_point) const = 0;


		virtual ~Shape();
	};
}

#endif // !MATH_SHAPES_H_
