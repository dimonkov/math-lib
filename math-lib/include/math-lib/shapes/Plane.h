#ifndef MATH_SHAPES_PLANE_H_  
#define MATH_SHAPES_PLANE_H_

#include "math-lib/shapes/Shape.h"

namespace math::shapes
{
	class Plane : public Shape
	{
	public:
		vector3 Normal;

		std::optional<double> intersect(const vector3& obj_origin, const ray& ray) const override;

		vector3 surface_normal(const vector3& obj_origin, const vector3& hit_point) const override;
	};
}

#endif // !MATH_SHAPES_PLANE_H_
