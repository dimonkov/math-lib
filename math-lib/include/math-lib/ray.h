#ifndef MATH_RAY_H  
#define MATH_RAY_H

#include "math-lib/vector3.h"

namespace math
{
	class ray
	{
	public:
		vector3 origin;
		vector3 direction;

		ray& operator=(const ray&) = default;
	};
}

#endif // !MATH_RAY_H
