#ifndef MATH_AREA_H_
#define MATH_AREA_H_


#include "math-lib/vector2.h"

namespace math
{
	//represents a rectangular area.
	//A - upper left corner, B - lower right corner
	class area
	{
	public:
		area();
		area(vector2 a, vector2 b);

		vector2 A, B;

		bool Intersects(vector2 coord) const;
	};
}
#endif // !MATH_AREA_H_