#ifndef MATH_VECTOR3_H_
#define MATH_VECTOR3_H_

namespace math
{
	//Three coordinates representing a point in 3D space
	class vector3
	{
	public:
		vector3(double x, double y, double z);
		vector3(double x, double y);
		vector3(double x);
		vector3();

		static const vector3 zero() { return vector3(0,0,0); }

		static const vector3 from_one(double n) { return vector3(n, n, n); }

		double X, Y, Z;

		vector3 operator+(const vector3& vec) const;
		vector3 operator-(const vector3& vec) const;
		vector3 operator-() const;

		vector3 operator*(double n) const;
		vector3 operator/(double n) const;

		double length() const;

		double sqr_length() const;

		vector3 normalize() const;

		double dot(const vector3& other);

		vector3 cross(const vector3& other);
	};
}

#endif // !MATH_VECTOR3_H_
