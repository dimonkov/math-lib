cmake_minimum_required(VERSION 3.8)

project(math-lib VERSION 1.0.0 LANGUAGES CXX)

# compile library
add_library(math-lib STATIC 

include/math-lib/area.h
src/area.cc

include/math-lib/vector2.h
src/vector2.cc

include/math-lib/vector3.h
src/vector3.cc

include/math-lib/ray.h
src/ray.cc

include/math-lib/shapes/Sphere.h
src/shapes/Sphere.cc

include/math-lib/shapes/Plane.h
src/shapes/Plane.cc

include/math-lib/shapes/Shape.h
src/shapes/Shape.cc

)

target_compile_features(math-lib PUBLIC cxx_std_17)

#Include own headers, expose headers for the others
target_include_directories(math-lib 
    PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/include
)